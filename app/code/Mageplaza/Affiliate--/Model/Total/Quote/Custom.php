<?php

namespace Mageplaza\Affiliate\Model\Total\Quote;
/**
 * Class Custom
 * @package Mageplaza\GiftCard\Model\Total\Quote
 */
class Custom extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;
    protected $_scopeConfig;
    protected $_checkoutSession;
    private $customCookieManager;
    protected $_cookie;


    /**
     * Custom constructor.
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Stdlib\CookieManagerInterface $customCookieManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Mageplaza\Affiliate\Controller\Refer\Index $setCookie
    )
    {
        $this->_priceCurrency = $priceCurrency;
        $this->_checkoutSession = $checkoutSession;
        $this->_scopeConfig = $scopeConfig;
        $this->customCookieManager = $customCookieManager;
        $this->_cookie = $setCookie;
    }

    public function getConfig($path){
        return $this->_scopeConfig->getValue($path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCookie($key)
    {
        return $this->customCookieManager->getCookie(
            $key
        );
    }
    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this|bool
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        $urlKey = 'affiliate/general/url_key';
        $discount = 'affiliate/affiliate_rule/select_discount';
        $discountValue = 'affiliate/affiliate_rule/discount_value';
        parent::collect($quote, $shippingAssignment, $total);

        $affiliateCode = $this->getCookie($this->getConfig($urlKey));
        $refer = $this->_checkoutSession->getData('refer_code');
        if ($affiliateCode || $refer) {
            if($this->getConfig($discount) == 1){
                $affiliateDiscount = 0;
                $total->addTotalAmount('customdiscount', $affiliateDiscount);
                $total->setCustomDiscount($affiliateDiscount);
                $quote->setCustomDiscount($affiliateDiscount);
            }elseif ($this->getConfig($discount) == 2){
                $affiliateDiscount = -$this->getConfig($discountValue);
                $this->_cookie->setCookie('discount', $affiliateDiscount);
                $total->addTotalAmount('customdiscount', $affiliateDiscount);
                $total->setCustomDiscount($affiliateDiscount);
                $quote->setCustomDiscount($affiliateDiscount);
            }else{
                $affiliateDiscount = -$quote->getSubtotal() * $this->getConfig($discountValue) / 100;
                $this->_cookie->setCookie('discount', $affiliateDiscount);
                $total->addTotalAmount('customdiscount', $affiliateDiscount);
                $total->setCustomDiscount($affiliateDiscount);
                $quote->setCustomDiscount($affiliateDiscount);
            }
        }


        return $this;
    }

    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $urlKey = 'affiliate/general/url_key';
        $discountValue = 'affiliate/affiliate_rule/commission_value';
        $affiliateCode = $this->getCookie($this->getConfig($urlKey));
        $refer = $this->_checkoutSession->getData('refer_code');
        if ($affiliateCode || $refer) {
            return [
                'code' => 'affiliate_discount',
                'title' => 'Affiliate Discount',
                'value' => $this->getCookie('discount')
            ];
        }else{
            return [
                'code' => 'affiliate_discount',
                'title' => 'Affiliate Discount',
                'value' => 0
            ];
        }

    }

}
