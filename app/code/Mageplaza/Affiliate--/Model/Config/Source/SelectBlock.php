<?php
namespace Mageplaza\Affiliate\Model\Config\Source;
use Magento\Framework\Option\ArrayInterface;
class SelectBlock implements ArrayInterface
{
    /**
     * Retrieve option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('Block 1')],
            ['value' => '2', 'label' => __('Block 2')],
            ['value' => '3', 'label' => __('Block 3')],
        ];
    }
}
