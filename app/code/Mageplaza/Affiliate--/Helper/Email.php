<?php
namespace Mageplaza\Affiliate\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;
    protected $_accountFactory;
    protected $_customerSession;
    protected $_checkoutSession;
    protected $_customerFactory;
    private $customCookieManager;

    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Magento\Framework\Stdlib\CookieManagerInterface $customCookieManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        Escaper $escaper,
        TransportBuilder $transportBuilder
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->customCookieManager = $customCookieManager;
        $this->escaper = $escaper;
        $this->_customerFactory = $customerFactory;
        $this->transportBuilder = $transportBuilder;
        $this->_accountFactory = $accountFactory;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->logger = $context->getLogger();
    }

    public function getKey(){
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\App\Config\ScopeConfigInterface::class)
            ->getValue(
                'affiliate/general/url_key',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }

    public function getCookie()
    {
        return $this->customCookieManager->getCookie(
            $this->getKey()
        );
    }

    public function sendEmail()
    {
        $account = $this->_accountFactory->create();
        if($this->getCookie()){
            $accountData = $account->load($this->getCookie(), 'code');
        }else{
            $accountData = $account->load($this->_checkoutSession->getData('refer_code'), 'code');
        }
        $customerId = $accountData->getData('customer_id');
        $customer = $this->_customerFactory->create()->load($customerId);
        try {
//            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $this->escaper->escapeHtml('Cam Van Chuc'),
                'email' => $this->escaper->escapeHtml('chuccv@mageplaza.com'),
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('email_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'templateVar'  => 'Order success, your balance is ' .'$'. $accountData->getData('balance'),
                ])
                ->setFrom($sender)
                ->addTo($customer->getData('email'))
                ->getTransport();
            $transport->sendMessage();
//            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
