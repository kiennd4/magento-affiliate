<?php
namespace Mageplaza\Affiliate\Controller\Account;

use Magento\Framework\App\Action\Context;

class Register extends \Magento\Framework\App\Action\Action
{
    protected $_accountFactory;
    protected $_customerSession;
    protected $resultFactory;
    protected $_scopeConfig;
    protected $_request;


    public function __construct(
        Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_customerSession = $customerSession;
        $this->resultFactory = $resultFactory;
        $this->_accountFactory = $accountFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_request = $request;
        parent::__construct($context);
    }

    protected $newCode;
    public function getGiftCode($length) {
        return $this->newCode = substr(str_shuffle('abcdefghijklmnopqrstuvxyz0123456789'),0 , $length);
    }

    public function execute()
    {
        $length = $this->_scopeConfig->getValue('affiliate/general/length',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $code = $this->getGiftCode($length);
        $customerId = $this->_customerSession->getCustomer()->getId();
        $model = $this->_accountFactory->create();

        $data = [
            'customer_id' => $customerId,
            'code' => $code,
            'balance' => 0,
            'status' => 1
        ];
        $model->addData($data);
        try {
            $model->save();
            $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $redirect->setUrl('/affiliate/account/index');
            return $redirect;
        }catch (\Exception $e){
            echo "Error";
        }


    }
}
