<?php
namespace Mageplaza\Affiliate\Controller\Refer;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_accountFactory;
    protected $resultFactory;
    protected $_request;
    private $customCookieMetadataFactory;
    private $customCookieManager;
    protected $_customerSession;


    public function __construct(
        Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Stdlib\CookieManagerInterface $customCookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $customCookieMetadataFactory,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\ResultFactory $resultFactory
    )
    {
        $this->resultFactory = $resultFactory;
        $this->_accountFactory = $accountFactory;
        $this->_customerSession = $customerSession;
        $this->customCookieMetadataFactory = $customCookieMetadataFactory;
        $this->customCookieManager = $customCookieManager;
        $this->_request = $request;
        parent::__construct($context);
    }

    public function getParamData()
    {
        return $this->_request->getParams();
    }

    public function setCookie($key,$value)
    {
        $customCookieMetadata = $this->customCookieMetadataFactory->createPublicCookieMetadata();
        $customCookieMetadata->setDurationOneYear();
        $customCookieMetadata->setPath('/');
        $customCookieMetadata->setHttpOnly(false);

        return $this->customCookieManager->setPublicCookie(
            $key,
            $value,
            $customCookieMetadata
        );
    }

    public function getCookie($key)
    {
        return $this->customCookieManager->getCookie(
            $key
        );
    }

    public function execute()
    {
        $model = $this->_accountFactory->create();

        if($this->getParamData()){
            $key = array_keys($this->getParamData())[0];
            $value = $this->getParamData()[$key];
            $customerId = $this->_customerSession->getCustomer()->getId();
            $checkReferCode = $model->load($customerId,'customer_id')->getData('code') === $value;
            $check = $model->load($value, 'code')->getData();
            if($check != null && $checkReferCode == false){
                $this->setCookie($key,$value);
                $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
                $redirect->setPath('');
                return $redirect;
            }
            $this->messageManager->addError('You can\'t refer code yourself');
            $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $redirect->setPath('affiliate/account/index');
            return $redirect;
        }
    }
}
