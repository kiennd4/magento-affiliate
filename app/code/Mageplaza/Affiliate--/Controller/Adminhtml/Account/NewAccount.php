<?php
namespace Mageplaza\Affiliate\Controller\Adminhtml\Account;

class NewAccount extends \Magento\Backend\App\Action
{

    protected $_resultPageFactory;

    protected $_resultForwardFactory;

    public function __construct(
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_resultForwardFactory = $resultForwardFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->accountFactory = $accountFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $resultPage = $this->_resultForwardFactory->create();
        $resultPage->forward('edit');
        return $resultPage;
    }
}
