<?php

namespace Mageplaza\Affiliate\Block\Adminhtml\Action\Edit\Tab;
use Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Main
 * @package Mageplaza\Affiliate\Block\Adminhtml\Action\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    protected $_paramId;
    protected $_accountFactory;
    public function __construct(
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Mageplaza\Affiliate\Controller\Adminhtml\Account\Edit $paramId,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry             $registry,
        \Magento\Framework\Data\FormFactory     $formFactory,
        array                                   $data = []
    )
    {
        $this->_paramId = $paramId;
        $this->_accountFactory = $accountFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        /** @var \Mageplaza\Affiliate\Model\ResourceModel $model  */
        $model = $this->_coreRegistry->registry('account_action');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('action_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Account Information'), 'class' => 'fieldset-wide']
        );

        if ($this->_paramId->getIddata()) {
            $fieldset->addField(
                'account_id',
                'hidden',
                ['name' => 'account_id'
                ]
            );
            $fieldset->addField(
                'customer_id',
                'text',
                ['name' => 'customer_id',
                    'label' => __('Customer Id'),
                    'title' => __('Customer Id'),
                    'disabled' => true,
                ]

            );
            $fieldset->addField(
                'customer_name',
                'text',
                ['name' => 'customer_id',
                    'label' => __('Customer Name'),
                    'title' => __('Customer Name'),
                    'disabled' => true,
                ]

            );
            $fieldset->addField(
                'code',
                'text',
                ['name' => 'code',
                    'label' => __('Code'),
                    'title' => __('Code'),
                    'disabled' => true,
                ]

            );
            $fieldset->addField(
                'status',
                'select',
                ['name' => 'status',
                    'label' => __('Status'),
                    'title' => __('Status'),
                    'class' => 'required-entry',
                    'required' => true,
                    'value' => 1,
                    'values'    => [
                        ['label' => 'Active', 'value' => 1],
                        ['label' => 'Inactive', 'value' => 0]
                    ]
                ]
            );
            $fieldset->addField(
                'balance',
                'text',
                ['name' => 'balance',
                    'label' => __('Balance'),
                    'title' => __('Balance')
                ]

            );
            $fieldset->addField(
                'created_at',
                'text',
                ['name' => 'created_at',
                    'label' => __('Create At'),
                    'title' => __('Create at'),
                    'disabled' => true,
                ]

            );
            $account = $this->_accountFactory->create();
            $id = $this->_paramId->getIddata();
            $data = $account->getCollection()->addFieldToFilter('account_id',$id)->getData();
            $model->setData('account_id',$id);
            $model->setData('customer_id',$data[0]['customer_id']);
            $model->setData('customer_name',$data[0]['lastname'] . ' ('. $data[0]['email']. ')');
            $model->setData('code',$data[0]['code']);
            $model->setData('balance',$data[0]['balance']);
            $model->setData('created_at',$data[0]['created_at']);

        }else {
            $fieldset->addField(
                'customer_id',
                'text',
                ['name' => 'customer_id',
                    'label' => __('Customer Id'),
                    'title' => __('Customer Id'),
                    'class' => 'validate-number'
                ]

            );

            $fieldset->addField(
                'status',
                'select',
                ['name' => 'status',
                    'label' => __('Status'),
                    'title' => __('Status'),
                    'class' => 'required-entry',
                    'required' => true,
                    'values'    => [
                        ['label' => 'Active', 'value' => 1],
                        ['label' => 'Inactive', 'value' => 0]
                    ]
                ]
            );
            $fieldset->addField(
                'balance',
                'text',
                ['name' => 'balance',
                    'label' => __('Initial Balance'),
                    'title' => __('Initial Balance'),
                    'class' => 'validate-number',
                    'required' => true]
            );
            $model->setData('create_from','admin');
        }








        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Return Tab label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Account Information');
    }
    /**
     * Return Tab title
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Account Information');
    }
    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}

