<?php

namespace Mageplaza\Affiliate\Block\Adminhtml;

class Account extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_account';
        $this->_blockGroup = 'Mageplaza_Affiliate';
        $this->_headerText = __('New Account');
        $this->_addButtonLabel = __('Save Account');
        parent::_construct();
    }
}
