<?php

namespace Mageplaza\Affiliate\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Quote\Model\Quote;
use Magento\Framework\Stdlib\CookieManagerInterface;

class AffiliateCommission implements ObserverInterface
{
    private $customCookieManager;
    protected $_accountFactory;
    protected $_historyFactory;
    protected $_checkoutSession;
    protected $_cookieMetadataFactory;
    protected $_registry;
    protected $_cookie;
    protected $_email;
    public function __construct(
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Stdlib\CookieManagerInterface $customCookieManager,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Mageplaza\Affiliate\Model\HistoryFactory $historyFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Registry $registry,
        \Mageplaza\Affiliate\Controller\Refer\Index $setCookie,
        \Mageplaza\Affiliate\Helper\Email $email
    )
    {
        $this->_accountFactory = $accountFactory;
        $this->customCookieManager = $customCookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_historyFactory = $historyFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_registry = $registry;
        $this->_email = $email;
        $this->_cookie = $setCookie;
    }

    public function getKey(){
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\App\Config\ScopeConfigInterface::class)
            ->getValue(
                'affiliate/general/url_key',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }

    public function getCookie()
    {
        return $this->customCookieManager->getCookie(
            $this->getKey()
        );
    }
    public function deleteCookie(){
        $metadata = $this->_cookieMetadataFactory->createPublicCookieMetadata();
        $metadata->setPath('/');
        return $this->customCookieManager->deleteCookie(
            $this->getKey(), $metadata
        );
    }
    public function getCommissionType(){
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\App\Config\ScopeConfigInterface::class)
            ->getValue(
                'affiliate/affiliate_rule/select_commission',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }
    public function getCommissionValue(){
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\App\Config\ScopeConfigInterface::class)
            ->getValue(
                'affiliate/affiliate_rule/commission_value',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }



    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getEvent()->getData('quote');

        $account = $this->_accountFactory->create();
        $history = $this->_historyFactory->create();
        $refer = $this->_checkoutSession->getData('refer_code');
        if( $this->getCookie() || $refer){
            if($this->getCookie()){
                $balance = $account->load($this->getCookie(), 'code');
            }else{
                $balance = $account->load($refer, 'code');
            }
            if($this->getCommissionType() == 1){
                $this->_registry->register('commission', $balance->getBalance() + $this->getCommissionValue());
                $balance->setBalance($balance->getBalance() + $this->getCommissionValue())->save();
            }elseif( $this->getCommissionType() == 2){
                $balance->setBalance($balance->getBalance() + ($quote->getData('base_subtotal') * $this->getCommissionValue()) / 100)->save();
                $this->_registry->register('commission', ($quote->getData('base_subtotal') * $this->getCommissionValue()) / 100);
            }else{
                $balance->setBalance($balance->getBalance() + 0)->save();
            }
            if($this->getCookie()){
                $customerId = $account->load($this->getCookie(), 'code')->getData('customer_id');
            }else{
                $customerId = $account->load($refer, 'code')->getData('customer_id');
            }
            $data =[
                'order_id' => $quote->getData('entity_id'),
                'order_increment_id' => $quote->getData('reserved_order_id'),
                'customer_id' => $customerId,
                'is_admin_change' => 0,
                'amount' => $this->_registry->registry('commission'),
                'status' => 1            ];

            $history->addData($data)->save();
            $this->_email->sendEmail();
            if($this->getCookie()){
                $this->deleteCookie();
            }
        }
    }
}
